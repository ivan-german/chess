package org.hillel.itschool.chess;

public class Horse implements Figure {

    @Override
    public boolean isValidMove(Board.Cell from, Board.Cell to) {
        if (from.equals(to)) return false;

        int rowOffset = Math.abs(from.row - to.row);
        int colOffset = Math.abs(from.col - to.col);

        return (rowOffset == 1 && colOffset == 2) || (rowOffset == 2 && colOffset == 1);
    }

}
