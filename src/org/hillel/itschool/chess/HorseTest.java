package org.hillel.itschool.chess;

import org.junit.Test;
import static org.junit.Assert.*;

public class HorseTest {

    @Test
    public void testIsValidMoveValidMove1() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('F', 6);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove2() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('G', 5);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove3() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('G', 3);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove4() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('F', 2);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove5() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('D', 2);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove6() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('C', 3);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove7() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('C', 5);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove8() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('D', 6);

        assertTrue(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveSameCell() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('E', 4);

        assertFalse(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveNeighborCell() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('D', 4);

        assertFalse(horse.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveCustomInvalidMove() {
        Figure horse = new Horse();

        Board.Cell from = new Board.Cell('E', 4);
        Board.Cell to = new Board.Cell('A', 1);

        assertFalse(horse.isValidMove(from, to));
    }

}
