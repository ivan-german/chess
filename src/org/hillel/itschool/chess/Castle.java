package org.hillel.itschool.chess;

public class Castle implements Figure {

    /**
     * Ладья может перемещаться только по прямым линиям.
     *
     * @param from начальное положение фигуры
     * @param to конечное положение фигуры
     *
     * @return true - если ход возможен, false в противном случае
     */
    @Override
    public boolean isValidMove(Board.Cell from, Board.Cell to) {
        boolean rowMove = from.row == to.row && from.col != to.col;
        boolean colMove = from.row != to.row && from.col == to.col;

        return rowMove || colMove;
    }
}
