package org.hillel.itschool.chess;

import org.junit.Test;
import static org.junit.Assert.*;

public class PawnTest {

    @Test
    public void testIsValidMoveValidMove() {
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('A', 1);
        Board.Cell to = new Board.Cell('B', 1);

        assertTrue(pawn.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveHorizontalMove() {
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('A', 1);
        Board.Cell to = new Board.Cell('A', 2);

        assertFalse(pawn.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveBackwardMove() {
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('B', 1);
        Board.Cell to = new Board.Cell('A', 1);

        assertFalse(pawn.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveCustomInvalidMove() {
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('B', 1);
        Board.Cell to = new Board.Cell('C', 6);

        assertFalse(pawn.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveSameCell() {
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('A', 0);
        Board.Cell to = new Board.Cell('A', 0);

        assertFalse(pawn.isValidMove(from, to));
    }

}
