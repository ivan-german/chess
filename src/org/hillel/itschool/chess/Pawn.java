package org.hillel.itschool.chess;

public class Pawn implements Figure {

    /**
     * Для простоты считаем, что пешки ходят только в одном направлении и
     * только на одну клетку. Пока все фигуры одного цвета это допустимо.
     *
     * @param from начальное положение фигуры
     * @param to конечное положение фигуры
     *
     * @return true - если ход возможен, false в противном случае
     */
    @Override
    public boolean isValidMove(Board.Cell from, Board.Cell to) {
        return (from.col == to.col) && (from.row == to.row - 1);
    }

}
