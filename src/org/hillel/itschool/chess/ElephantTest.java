package org.hillel.itschool.chess;

import org.junit.Test;
import static org.junit.Assert.*;

public class ElephantTest {
    
    @Test
    public void testIsValidMoveValidMove1() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('E', 5);
        
        assertTrue(elephant.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove2() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('E', 1);

        assertTrue(elephant.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove3() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('A', 1);

        assertTrue(elephant.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove4() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('A', 5);

        assertTrue(elephant.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveInvalidMoveSameRow() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('C', 6);

        assertFalse(elephant.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveInvalidMoveSameCol() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('E', 3);

        assertFalse(elephant.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveInvalidMoveSameCell() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('C', 3);

        assertFalse(elephant.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveCustomInvalidMove() {
        Figure elephant = new Elephant();

        Board.Cell from = new Board.Cell('C', 3);
        Board.Cell to = new Board.Cell('H', 1);

        assertFalse(elephant.isValidMove(from, to));
    }

}
