package org.hillel.itschool.chess;

// Шахматная доска
public class Board {

    private Figure[][] field = new Figure[8][8];

    /**
     * Метод ставит фигуру на поле. фигуру нельзя установить за поле, на
     * клетку, где уже стоит фигура или если указанная фигура уже стоит на доске.
     *
     * @param figure фигура, которую нужно установить
     * @param cell клетка, на которую нужно установить фигуру
     *
     * @return true - если удалось устаовить фигуру, false в противном случае
     */
    public boolean placeFigure(Figure figure, Cell cell) {
        if (isCellOutsideField(cell)) return false;
        if (getCell(cell) != null) return false;
        if (isFigureOnBoard(figure)) return false;

        field[cell.row][cell.col] = figure;
        return true;
    }

    /**
     * Метод перемещает фигуру с одной клетки на другую. Нельзя перемещать
     * фигуру за пределы поля, на клетку с другой фигурой или против правил
     * хода фигури в указанной клетке.
     *
     * @param from клетка, с которой перемещают фигуру
     * @param to клетка, на которую перемещают фигуру
     *
     * @return true - если перемещение было удачным, false в противном случае
     */
    public boolean moveFigure(Cell from, Cell to) {
        if (isCellOutsideField(from) || isCellOutsideField(to)) return false;

        Figure figure = getCell(from);
        if (figure == null) return false;
        if (getCell(to) != null) return false;
        if (!figure.isValidMove(from, to)) return false;

        takeFigure(from);
        placeFigure(figure, to);
        return true;
    }

    /**
     * Убирает фигуру с поля
     *
     * @param cell клетка, с которой нужно убрать фигуру
     */
    public void takeFigure(Cell cell) {
        if (isCellOutsideField(cell)) return;

        field[cell.row][cell.col] = null;
    }

    /**
     * Получить доступ к фигуре, находящейся в указанной клетка
     *
     * @param cell клетка
     *
     * @return объект фигури, если та стоит на клетке, в противном случае null
     */
    public Figure getCell(Cell cell) {
        return field[cell.row][cell.col];
    }


    // Приватная часть класса. Тестированию не подлежит


    private boolean isFigureOnBoard(Figure figure) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == figure) return true;
            }
        }
        return false;
    }

    private boolean isCellOutsideField(Cell cell) {
        return cell.row >= field.length || cell.col >= field.length;
    }


    /**
     * Клетка шахматного поля. Для создания используется шахматная нотация: А1, В4 и т.д.
     */
    public static class Cell {
        final int row;
        final int col;

        public Cell(char row, int col) {
            this.row = row - 'A';
            this.col = col;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Cell cell = (Cell) o;

            if (col != cell.col) return false;
            if (row != cell.row) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = row;
            result = 31 * result + col;
            return result;
        }
    }

}
