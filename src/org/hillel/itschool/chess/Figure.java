package org.hillel.itschool.chess;

public interface Figure {
    /**
     * Проверка, что фигура может переместиться между указанными клетками. Ход на
     * ту же клетку, что и начальная считается некорректным.
     *
     * @param from начальное положение фигуры
     * @param to конечное положение фигуры
     *
     * @return true - если ход возможен, false в противном случае
     */
    boolean isValidMove(Board.Cell from, Board.Cell to);
}
