package org.hillel.itschool.chess;

import org.junit.Test;
import static org.junit.Assert.*;

public class CastleTest {

    @Test
    public void testIsValidMoveValidMove1() {
        Figure castle = new Castle();

        Board.Cell from = new Board.Cell('A', 0);
        Board.Cell to = new Board.Cell('A', 7);

        assertTrue(castle.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove2() {
        Figure castle = new Castle();

        Board.Cell from = new Board.Cell('A', 0);
        Board.Cell to = new Board.Cell('H', 0);

        assertTrue(castle.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove3() {
        Figure castle = new Castle();

        Board.Cell from = new Board.Cell('A', 7);
        Board.Cell to = new Board.Cell('A', 0);

        assertTrue(castle.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveValidMove4() {
        Figure castle = new Castle();

        Board.Cell from = new Board.Cell('H', 0);
        Board.Cell to = new Board.Cell('A', 0);

        assertTrue(castle.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveCustomInvalidMove() {
        Figure castle = new Castle();

        Board.Cell from = new Board.Cell('A', 0);
        Board.Cell to = new Board.Cell('B', 7);

        assertFalse(castle.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveNeighborCell() {
        Figure castle = new Castle();

        Board.Cell from = new Board.Cell('A', 0);
        Board.Cell to = new Board.Cell('B', 1);

        assertFalse(castle.isValidMove(from, to));
    }

    @Test
    public void testIsValidMoveSameCell() {
        Figure castle = new Castle();

        Board.Cell from = new Board.Cell('A', 0);
        Board.Cell to = new Board.Cell('A', 0);

        assertFalse(castle.isValidMove(from, to));
    }






}
