package org.hillel.itschool.chess;

import org.junit.Test;
import static org.junit.Assert.*;

public class BoardTest {

    @Test
    public void testPlaceFigure() {
        Board board = new Board();
        Figure pawn = new Pawn();
        Board.Cell cell = new Board.Cell('A', 1);

        assertTrue(board.placeFigure(pawn, cell));
    }

    @Test
    public void testPlaceFigureSameCell() {
        Board board = new Board();
        Figure pawn1 = new Pawn();
        Figure pawn2 = new Pawn();
        Board.Cell cell = new Board.Cell('A', 1);

        board.placeFigure(pawn1, cell);

        assertFalse(board.placeFigure(pawn2, cell));
    }

    @Test
    public void testPlaceFigureSameFigure() {
        Board board = new Board();
        Figure pawn = new Pawn();

        Board.Cell cell1 = new Board.Cell('A', 1);
        Board.Cell cell2 = new Board.Cell('B', 4);

        board.placeFigure(pawn, cell1);

        assertFalse(board.placeFigure(pawn, cell2));
    }

    @Test
    public void testPlaceFigureFieldChanged() {
        Board board = new Board();
        Figure pawn = new Pawn();

        Board.Cell cell = new Board.Cell('A', 1);

        board.placeFigure(pawn, cell);

        assertEquals(pawn, board.getCell(cell));
    }

    @Test
    public void testTakeFigureFieldChanged() {
        Board board = new Board();
        Figure pawn = new Pawn();

        Board.Cell cell = new Board.Cell('A', 1);

        board.placeFigure(pawn, cell);
        board.takeFigure(cell);

        assertNull(board.getCell(cell));
    }

    @Test
    public void testMoveFigureValidMove() {
        Board board = new Board();
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('A', 1);
        Board.Cell to = new Board.Cell('B', 1);

        board.placeFigure(pawn, from);
        boolean result = board.moveFigure(from, to);

        assertTrue(result);
        assertNull(board.getCell(from));
        assertEquals(pawn, board.getCell(to));
    }

    @Test
    public void testMoveFigureInvalidMove() {
        Board board = new Board();
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('A', 1);
        Board.Cell to = new Board.Cell('D', 3);

        board.placeFigure(pawn, from);
        boolean result = board.moveFigure(from, to);

        assertFalse(result);
        assertEquals(pawn, board.getCell(from));
        assertNull(board.getCell(to));
    }

    @Test
    public void testMoveFigureOutsideField() {
        Board board = new Board();
        Figure pawn = new Pawn();

        Board.Cell from = new Board.Cell('A', 1);
        Board.Cell to = new Board.Cell('A', 42);

        board.placeFigure(pawn, from);
        boolean result = board.moveFigure(from, to);

        assertFalse(result);
    }

    @Test
    public void testMoveFigureOccupiedField() {
        Board board = new Board();
        Figure pawn1 = new Pawn();
        Figure pawn2 = new Pawn();


        Board.Cell from = new Board.Cell('A', 1);
        Board.Cell to = new Board.Cell('B', 1);

        board.placeFigure(pawn1, from);
        board.placeFigure(pawn2, to);

        boolean result = board.moveFigure(from, to);
        assertFalse(result);
    }

}
