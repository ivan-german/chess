package org.hillel.itschool.chess;

public class Elephant implements Figure {

    /**
     * Слон перемещается только по диагонали.
     *
     * @param from начальное положение фигуры
     * @param to конечное положение фигуры
     *
     * @return true - если ход возможен, false в противном случае
     */
    @Override
    public boolean isValidMove(Board.Cell from, Board.Cell to) {
        if (from.equals(to)) return false;

        int rowOffset = from.row - to.row;
        int colOffset = from.col - to.col;

        return rowOffset == colOffset || rowOffset == -colOffset;
    }
}
